package com.example.android.androidassignment.view;

import android.content.Context;

public interface BaseView {
    Context getContext();

    void showProgressDialog();

    void hideProgressDialog();

    void displayToastMessage(String message);
}
