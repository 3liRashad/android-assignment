package com.example.android.androidassignment.di.component;

import com.example.android.androidassignment.di.module.HomeModule;
import com.example.android.androidassignment.di.scope.PerActivity;
import com.example.android.androidassignment.view.activity.HomeActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {HomeModule.class})
public interface HomeComponent {
    void inject(HomeActivity homeActivity);
}
