package com.example.android.androidassignment.network;

import android.content.Context;

import com.example.android.androidassignment.BuildConfig;
import com.example.android.androidassignment.api.CoursesApi;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {
    private static final long REQUEST_TIMEOUT_MILLISEC = 720 * 1000;
    private static final String HTTP = "http";
    private static final int HTTP_CACHE = 10 * 1024 * 1024; // 10 MiB
    private static NetworkManager instance;
    private Retrofit retrofit;

    private NetworkManager(Context context) {
        provideRetrofit(provideGsonConverterFactory(),
                provideRxJava2CallAdapterFactory(),
                getOkHttpClient(context));
    }

    public static NetworkManager getInstance(Context context) {
        if (instance == null)
            instance = new NetworkManager(context);
        return instance;
    }

    public static NetworkManager getInstance() {
        return instance;
    }

    private void provideRetrofit(GsonConverterFactory gsonConverterFactory,
                                 RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
                                 OkHttpClient okHttpClient) {
        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .baseUrl(BuildConfig.API_BASE_URL)
                .build();
    }

    private GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    private RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    private OkHttpClient getOkHttpClient(Context context) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        setLoggingInterceptor(clientBuilder);
        setHeadersInterceptor(clientBuilder);
        clientBuilder.cache(getHttpCache(context));
        setTimeouts(clientBuilder);
        return clientBuilder.build();
    }

    private void setTimeouts(OkHttpClient.Builder clientBuilder) {
        clientBuilder.connectTimeout(REQUEST_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS);
        clientBuilder.readTimeout(REQUEST_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS);
        clientBuilder.writeTimeout(REQUEST_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS);
    }


    private void setLoggingInterceptor(OkHttpClient.Builder clientBuilder) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(logging);
    }

    private void setHeadersInterceptor(OkHttpClient.Builder clientBuilder) {
        clientBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            HttpUrl.Builder httpUrl = original.url().newBuilder();

            Request request = requestBuilder.url(httpUrl.build())
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });
    }

    private Cache getHttpCache(Context context) {
        final File httpCacheDir = new File(context.getCacheDir(), HTTP);
        final long httpCacheSize = HTTP_CACHE;
        return new Cache(httpCacheDir, httpCacheSize);
    }

    public CoursesApi provideCoursesApi() {
        return retrofit.create(CoursesApi.class);
    }

}
