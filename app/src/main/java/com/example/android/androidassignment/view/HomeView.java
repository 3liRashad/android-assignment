package com.example.android.androidassignment.view;

import com.example.android.androidassignment.model.CourseModel;
import com.example.android.androidassignment.model.SubjectModel;

import java.util.ArrayList;

public interface HomeView extends BaseView {
    void addItems(ArrayList<SubjectModel> subjects, ArrayList<CourseModel> courses);
}
