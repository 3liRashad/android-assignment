package com.example.android.androidassignment.di.component;

import com.example.android.androidassignment.SchoolApplication;
import com.example.android.androidassignment.di.module.InteractorModule;
import com.example.android.androidassignment.model.interactor.HomeInteractor;
import com.example.android.androidassignment.view.HomeView;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {InteractorModule.class})
public interface ApplicationComponent {

    void inject(SchoolApplication schoolApplication);

    HomeInteractor provideHomeInteractor();
}
