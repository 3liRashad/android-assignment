package com.example.android.androidassignment.callbacks;

public interface OnSubjectClicked {
    void onSubjectClicked(int position);
}
