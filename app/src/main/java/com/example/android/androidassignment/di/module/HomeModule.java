package com.example.android.androidassignment.di.module;

import com.example.android.androidassignment.di.scope.PerActivity;
import com.example.android.androidassignment.model.interactor.HomeInteractor;
import com.example.android.androidassignment.presenter.HomePresenter;
import com.example.android.androidassignment.presenter.HomePresenterImp;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    @PerActivity
    @Provides
    HomePresenter provideHomePresenter(HomeInteractor homeInteractor) {
        return new HomePresenterImp(homeInteractor);
    }
}
