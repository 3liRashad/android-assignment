package com.example.android.androidassignment.presenter;

import com.example.android.androidassignment.model.CourseModel;
import com.example.android.androidassignment.model.SubjectModel;
import com.example.android.androidassignment.model.interactor.HomeInteractor;
import com.example.android.androidassignment.model.reponse.GetSubjectsResponse;
import com.example.android.androidassignment.view.HomeView;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.example.android.androidassignment.utils.Constants.ERROR_MESSAGE;

public class HomePresenterImp extends HomePresenter {

    private ArrayList<SubjectModel> subjects;
    private ArrayList<ArrayList<CourseModel>> courses;

    public HomePresenterImp(HomeInteractor homeInteractor) {
        super(homeInteractor);
    }

    @Override
    public void onCreate(HomeView view) {
        super.onCreate(view);
        subjects = new ArrayList<>();
        courses = new ArrayList<>();
    }

    @Override
    public void onViewCreated() {
        getView().showProgressDialog();
        addSubscription(getInteractor().getCourses()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleError(Throwable error) {
        getView().displayToastMessage(ERROR_MESSAGE);
        getView().hideProgressDialog();
    }

    private void handleResponse(Response<GetSubjectsResponse> response) {
        for (int i = 0; i < response.body().getSubjects().size(); i++) {
            subjects.add(new SubjectModel(response.body().getSubjects().get(i).getSubjectTitle()));
            courses.add(response.body().getSubjects().get(i).getCourseDetailsResponse());
        }
        subjects.get(0).setSelected(true);
        getView().addItems(subjects, courses.get(0));
        getView().hideProgressDialog();
    }

    @Override
    public void onSubjectClicked(int position) {
        setSelectedSubject(position);
        getView().addItems(subjects, courses.get(position));
    }

    private void setSelectedSubject(int position) {
        for (int i = 0; i < subjects.size(); i++) {
            if (i == position)
                subjects.get(i).setSelected(true);
            else
                subjects.get(i).setSelected(false);
        }
    }
}
