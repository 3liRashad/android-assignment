package com.example.android.androidassignment.api;

import com.example.android.androidassignment.model.reponse.GetSubjectsResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

public interface CoursesApi {
    String GET_COURSES = "courses";

    @GET(GET_COURSES)
    Observable<Response<GetSubjectsResponse>> getCourses();
}
