package com.example.android.androidassignment.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.androidassignment.R;
import com.example.android.androidassignment.model.CourseModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.CourseViewHolder> {
    private ArrayList<CourseModel> courses;

    public CoursesAdapter(ArrayList<CourseModel> courses) {
        this.courses = courses;
    }

    @NonNull
    @Override
    public CoursesAdapter.CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CourseViewHolder(getCourseViewLayout(parent));
    }

    private View getCourseViewLayout(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_course, parent, false);
    }

    public void addItems(ArrayList<CourseModel> courses) {
        this.courses.clear();
        this.courses.addAll(courses);
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull CoursesAdapter.CourseViewHolder holder, int position) {
        holder.courseInstructor.setText(courses.get(position).getName());
        holder.startingDate.setText(courses.get(position).getStartingDate());
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class CourseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_view_course_instructor)
        TextView courseInstructor;
        @BindView(R.id.text_view_starting_date)
        TextView startingDate;

        public CourseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
