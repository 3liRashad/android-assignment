package com.example.android.androidassignment.model.interactor;

import com.example.android.androidassignment.network.NetworkManager;
import com.example.android.androidassignment.model.reponse.GetSubjectsResponse;

import io.reactivex.Observable;
import retrofit2.Response;

public class HomeInteractorImp extends HomeInteractor {
    public Observable<Response<GetSubjectsResponse>> getCourses() {
        return NetworkManager.getInstance().provideCoursesApi().getCourses();
    }
}
