package com.example.android.androidassignment.model.reponse;

import com.example.android.androidassignment.model.CourseModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SubjectsResponse {

    @SerializedName("title")
    private String subjectTitle;

    @SerializedName("courses")
    private ArrayList<CourseModel> courseDetailsResponse;

    public String getSubjectTitle() {
        return subjectTitle;
    }

    public ArrayList<CourseModel> getCourseDetailsResponse() {
        return courseDetailsResponse;
    }
}
