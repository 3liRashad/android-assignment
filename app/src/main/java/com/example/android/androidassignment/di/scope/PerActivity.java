package com.example.android.androidassignment.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

// Custom scope to
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
