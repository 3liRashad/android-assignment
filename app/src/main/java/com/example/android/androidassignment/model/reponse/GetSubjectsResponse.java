package com.example.android.androidassignment.model.reponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetSubjectsResponse {
    @SerializedName("subjects")
    private ArrayList<SubjectsResponse> subjects;

    public ArrayList<SubjectsResponse> getSubjects() {
        return subjects;
    }
}
