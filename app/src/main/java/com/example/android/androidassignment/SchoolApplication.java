package com.example.android.androidassignment;

import android.app.Application;

import com.example.android.androidassignment.di.component.ApplicationComponent;
import com.example.android.androidassignment.di.component.DaggerApplicationComponent;
import com.example.android.androidassignment.network.NetworkManager;

public class SchoolApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        NetworkManager.getInstance(this);
        injectApp();
    }

    private void injectApp() {
        applicationComponent = DaggerApplicationComponent.builder()
                .build();

        applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
