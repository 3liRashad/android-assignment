package com.example.android.androidassignment.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android.androidassignment.SchoolApplication;
import com.example.android.androidassignment.di.HasComponent;
import com.example.android.androidassignment.di.component.ApplicationComponent;
import com.example.android.androidassignment.presenter.BasePresenter;
import com.example.android.androidassignment.view.BaseView;

import javax.inject.Inject;

public abstract class BaseActivity<P extends BasePresenter, V extends BaseView, C>
        extends AppCompatActivity implements BaseView, HasComponent<C> {

    private C component;

    @Inject
    P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        component = initComponent();
        inject();
        super.onCreate(savedInstanceState);
        setContentView(getViewResourceId());
        if (presenter != null)
            presenter.onCreate((V) this);
    }

    protected abstract C initComponent();

    protected abstract void inject();

    protected P getPresenter() {
        return presenter;
    }

    protected abstract int getViewResourceId();

    @Override
    public C getComponent() {
        return component;
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((SchoolApplication) getApplication()).getApplicationComponent();
    }
}
