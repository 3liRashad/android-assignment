package com.example.android.androidassignment.model;

import com.google.gson.annotations.SerializedName;

public class CourseModel {
    @SerializedName("teacher_name")
    private String name;

    @SerializedName("starting_date")
    private String startingDate;

    public CourseModel(String name, String startingDate) {
        this.name = name;
        this.startingDate = startingDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }
}
