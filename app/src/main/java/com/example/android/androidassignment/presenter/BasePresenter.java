package com.example.android.androidassignment.presenter;

import com.example.android.androidassignment.model.interactor.BaseInteractor;
import com.example.android.androidassignment.view.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<V extends BaseView, I extends BaseInteractor> {
    I interactor;

    WeakReference<V> viewReference;

    private CompositeDisposable compositeDisposable;

    BasePresenter(I interactor) {
        this.interactor = interactor;
    }

    public void onCreate(V view) {
        attachView(view);
    }

    private void attachView(V view) {
        this.viewReference = new WeakReference<V>(view);
    }

    public I getInteractor() {
        return interactor;
    }

    public V getView() {
        return viewReference.get();
    }

    protected boolean isViewAttached() {
        return viewReference != null && viewReference.get() != null;
    }

    protected void addSubscription(Disposable disposable) {
        if (compositeDisposable == null)
            compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(disposable);
    }
}
