package com.example.android.androidassignment.model.interactor;

import com.example.android.androidassignment.model.reponse.GetSubjectsResponse;

import io.reactivex.Observable;
import retrofit2.Response;

public abstract class HomeInteractor extends BaseInteractor{

    public abstract Observable<Response<GetSubjectsResponse>> getCourses();
}
