package com.example.android.androidassignment.di.module;

import com.example.android.androidassignment.model.interactor.HomeInteractor;
import com.example.android.androidassignment.model.interactor.HomeInteractorImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class InteractorModule {

    @Singleton
    @Provides
    HomeInteractor provideHomeInteractor() {
        return new HomeInteractorImp();
    }
}
