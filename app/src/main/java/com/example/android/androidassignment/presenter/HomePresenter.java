package com.example.android.androidassignment.presenter;

import com.example.android.androidassignment.model.interactor.HomeInteractor;
import com.example.android.androidassignment.view.HomeView;

public abstract class HomePresenter extends BasePresenter<HomeView, HomeInteractor> {

    HomePresenter(HomeInteractor homeInteractor) {
        super(homeInteractor);
    }

    public abstract void onSubjectClicked(int position);

    public abstract void onViewCreated();
}
