package com.example.android.androidassignment.di;

public interface HasComponent<C> {
    C getComponent();
}
