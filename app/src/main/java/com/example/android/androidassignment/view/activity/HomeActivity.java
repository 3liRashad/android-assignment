package com.example.android.androidassignment.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.androidassignment.R;
import com.example.android.androidassignment.callbacks.OnSubjectClicked;
import com.example.android.androidassignment.di.component.DaggerHomeComponent;
import com.example.android.androidassignment.di.component.HomeComponent;
import com.example.android.androidassignment.di.module.HomeModule;
import com.example.android.androidassignment.model.CourseModel;
import com.example.android.androidassignment.model.SubjectModel;
import com.example.android.androidassignment.presenter.HomePresenter;
import com.example.android.androidassignment.view.HomeView;
import com.example.android.androidassignment.view.adapter.CoursesAdapter;
import com.example.android.androidassignment.view.adapter.SubjectsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity<HomePresenter, HomeView, HomeComponent> implements HomeView, OnSubjectClicked {

    @BindView(R.id.toolbar_home)
    Toolbar topToolbar;
    @BindView(R.id.recycler_view_subjects)
    RecyclerView subjectRecyclerView;
    @BindView(R.id.recycler_view_courses)
    RecyclerView coursesRecyclerView;
    @BindView(R.id.layout_progress_bar)
    RelativeLayout progressBar;

    private SubjectsAdapter subjectsAdapter;
    private CoursesAdapter coursesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        topToolbar.setTitle("");
        setSupportActionBar(topToolbar);
        initViews();
        getPresenter().onViewCreated();
    }

    private void initViews() {
        initSubjectsRecycler();
        initCoursesRecycler();
    }

    private void initSubjectsRecycler() {
        RecyclerView.LayoutManager subjectsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        subjectRecyclerView.setHasFixedSize(true);
        subjectRecyclerView.setLayoutManager(subjectsLayoutManager);
        ArrayList<SubjectModel> subjects = new ArrayList<>();
        subjectsAdapter = new SubjectsAdapter(this, subjects);
        subjectRecyclerView.setAdapter(subjectsAdapter);
    }

    private void initCoursesRecycler() {
        RecyclerView.LayoutManager coursesLayoutManager = new LinearLayoutManager(this);
        coursesRecyclerView.setHasFixedSize(true);
        coursesRecyclerView.setLayoutManager(coursesLayoutManager);
        ArrayList<CourseModel> courses = new ArrayList<>();
        coursesAdapter = new CoursesAdapter(courses);
        coursesRecyclerView.setAdapter(coursesAdapter);
    }

    @Override
    protected HomeComponent initComponent() {
        return DaggerHomeComponent.builder()
                .applicationComponent(getApplicationComponent())
                .homeModule(new HomeModule())
                .build();
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected int getViewResourceId() {
        return R.layout.activity_home;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void addItems(ArrayList<SubjectModel> subjects, ArrayList<CourseModel> courses) {
        subjectsAdapter.addItems(subjects);
        coursesAdapter.addItems(courses);
    }

    @Override
    public void onSubjectClicked(int position) {
        getPresenter().onSubjectClicked(position);
    }
}
