package com.example.android.androidassignment.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.androidassignment.R;
import com.example.android.androidassignment.callbacks.OnSubjectClicked;
import com.example.android.androidassignment.model.SubjectModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.SubjectViewHolder> {

    private ArrayList<SubjectModel> subjects;
    private OnSubjectClicked callback;

    public SubjectsAdapter(OnSubjectClicked callback, ArrayList<SubjectModel> subjects) {
        this.subjects = subjects;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SubjectsAdapter.SubjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubjectViewHolder(getSubjectViewLayout(parent));
    }

    private View getSubjectViewLayout(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_subject, parent, false);
    }

    public void addItems(ArrayList<SubjectModel> subjects) {
        this.subjects.clear();
        this.subjects.addAll(subjects);
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectsAdapter.SubjectViewHolder holder, int position) {
        holder.title.setText(subjects.get(position).getName());
        holder.itemView.setTag(position);
        if (subjects.get(position).isSelected()) {
            holder.title.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.cardView.setCardBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.blue));
        } else {
            holder.title.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.black));
            holder.cardView.setCardBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }

    public class SubjectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_title)
        TextView title;
        @BindView(R.id.card_view_item)
        CardView cardView;

        public SubjectViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> {
                callback.onSubjectClicked((Integer) view.getTag());
            });
        }
    }
}
